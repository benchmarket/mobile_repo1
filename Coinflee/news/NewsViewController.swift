//
//  NewsViewController.swift
//  Bitcoin
//
//  Created by Alex on 01/02/2019.
//  Copyright © 2019 Kevin. All rights reserved.
//

import UIKit
import SafariServices
import MBProgressHUD
import FeedKit

var feedsSelected: [Bool] = []

class RSSFeedItemExt {
    public var item: RSSFeedItem?
    public var feedTitle: String?
}
class NewsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate , XMLParserDelegate {
    var language = LanguageFile()
    var isReloading = false
    
    var myFeed : NSArray = []
    var myTitle : NSArray = []
    var feedImgs: [AnyObject] = []
    
    var parser = XMLParser()
    var currentElement:String = ""
    var foundCharacters = ""
    var passData:Bool=false
//    var parsedData = [[String:String]]()
    var parsedData: [RSSFeedItemExt] = []
    var currentData = [String:String]()
    var isHeader = true
    
    var loadNumber = 0
    var featureCount = 4
    
    var _imageList: [String: UIImage] = [:]
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var Latest: UINavigationItem!
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        Latest.title = language.localizedString(str: "Latest News")
        
        let indexPath = IndexPath(row: 0, section: 0)
        tableView.scrollToRow(at: indexPath, at: .top, animated: true)
        
        self.showHUD()
        self.loadData()
    }
    
  
    override func viewDidLoad() {
        super.viewDidLoad()

        setupTitleView()
        
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.rowHeight = 120
        
        if feedsSelected.count == 0 {
            for _ in 0...3 {
                feedsSelected.append(true)
            }
        }
   
    }
    
    func showHUD(){
        
        if let parentController = self.parent?.parent as? UITabBarController {
            MBProgressHUD.showAdded(to: (parentController.view)!, animated: true)
        }
    }
    
    @IBAction func goCoinbase(_ sender: Any) {
//        let urlString = "https://coinbase-consumer.sjv.io/9nJy3"
//
//        let config = SFSafariViewController.Configuration()
//        config.entersReaderIfAvailable = true
//        let url = URL(string: urlString)!
//        let vc = SFSafariViewController(url: url, configuration: config)
//        vc.preferredBarTintColor = UIColor(red: 3.0/255,green: 73.0/255,blue: 184.0/255,alpha: 1.0)
//        vc.preferredControlTintColor = UIColor.white
//        vc.delegate = self as? SFSafariViewControllerDelegate
//        self.present(vc, animated: true,completion: nil)
        guard let url = URL(string: "https://coinbase-consumer.sjv.io/9nJy3") else {
            return
        }
        UIApplication.shared.open(url)
    }
    
    @IBAction func goRobinhood(_ sender: Any) {
//        let urlString = "https://referral.robinhood.com/yanniem"
//
//        let config = SFSafariViewController.Configuration()
//        config.entersReaderIfAvailable = true
//        let url = URL(string: urlString)!
//        let vc = SFSafariViewController(url: url, configuration: config)
//        vc.preferredBarTintColor = UIColor(red: 3.0/255,green: 73.0/255,blue: 184.0/255,alpha: 1.0)
//        vc.preferredControlTintColor = UIColor.white
//        vc.delegate = self as? SFSafariViewControllerDelegate
//        self.present(vc, animated: true,completion: nil)
        guard let url = URL(string: "https://referral.robinhood.com/yanniem") else {
            return
        }
        UIApplication.shared.open(url)
    }
    @IBAction func goChangelly(_ sender: Any) {
//        let urlString = "https://old.changelly.com/?ref_id=qso2bg0jqpzvom2n"
//
//        let config = SFSafariViewController.Configuration()
//        config.entersReaderIfAvailable = true
//        let url = URL(string: urlString)!
//        let vc = SFSafariViewController(url: url, configuration: config)
//        vc.preferredBarTintColor = UIColor(red: 3.0/255,green: 73.0/255,blue: 184.0/255,alpha: 1.0)
//        vc.preferredControlTintColor = UIColor.white
//        vc.delegate = self as? SFSafariViewControllerDelegate
//        self.present(vc, animated: true,completion: nil)
        guard let url = URL(string: "https://old.changelly.com/?ref_id=qso2bg0jqpzvom2n") else {
            return
        }
        UIApplication.shared.open(url)
    }
    
    
    func dismissHUD(isAnimated:Bool) {
       
        if let parentController = self.parent?.parent as? UITabBarController {
            MBProgressHUD.hide(for: (parentController.view)!, animated: isAnimated)
        }
    }
    
    func processAfterFeedsLoad() {
        DispatchQueue.main.async {
           
            self.parsedData.sort(by: {
                guard let item_0 = $0.item,
                    let item_1 = $1.item,
                    let date_0 = item_0.pubDate,
                    let date_1 = item_1.pubDate else {
                        return false
                }
                return date_0.compare(date_1) == .orderedDescending
            })
            
           
            if self.isReloading == true {
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1) {
                    self.isReloading = false
                    self.tableView.tableHeaderView = nil
                }
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1.1) {
                    self.tableView.reloadData()
                }
            } else {
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.6, execute: {
                    self.dismissHUD(isAnimated: true)
                    self.tableView.reloadData()
                })
            }
        }
    }
    
    func loadData() {

        parsedData = []
        loadNumber = 0
        featureCount = 0
        
        for value in feedsSelected {
            if value {
                featureCount += 1
            }
        }
        
        if featureCount == 0 {
            self.dismissHUD(isAnimated: true)
            self.tableView.reloadData()
            return
        }
        
        if feedsSelected[0] {
            let url = URL(string: "http://bitcoinist.com/feed/")!
            let parser = FeedParser(URL: url)
                        parser.parseAsync(queue: DispatchQueue.global(qos: .userInitiated)) { (result) in
                guard let feed = result.rssFeed, result.isSuccess else {
                    print(result.error!)
                    return
                }
                if let items = feed.items {
                    for item in items {
                        let itemExt = RSSFeedItemExt()
                        itemExt.item = item
                        itemExt.feedTitle = feed.title
                        self.parsedData.append(itemExt)
                    }
                }
                            
                self.loadNumber += 1
                if self.loadNumber == self.featureCount {
                    self.processAfterFeedsLoad()
                }
            }
            
//            loadRss(url)
        }
        if feedsSelected[1] {
            let url = URL(string: "http://news.bitcoin.com/feed")!
            let parser = FeedParser(URL: url)
            parser.parseAsync(queue: DispatchQueue.global(qos: .userInitiated)) { (result) in
                guard let feed = result.rssFeed, result.isSuccess else {
                    print(result.error!)
                    return
                }
                if let items = feed.items {
                    for item in items {
                        let itemExt = RSSFeedItemExt()
                        itemExt.item = item
                        itemExt.feedTitle = feed.title
                        self.parsedData.append(itemExt)
                    }
                }
                
                self.loadNumber += 1
                if self.loadNumber == self.featureCount {
                    self.processAfterFeedsLoad()
                }
            }
//            loadRss(url)
        }
        if feedsSelected[2] {
            let url = URL(string: "http://www.coindesk.com/feed/")!
            let parser = FeedParser(URL: url)
            parser.parseAsync(queue: DispatchQueue.global(qos: .userInitiated)) { (result) in
                guard let feed = result.rssFeed, result.isSuccess else {
                    print(result.error!)
                    return
                }
                if let items = feed.items {
                    for item in items {
                        let itemExt = RSSFeedItemExt()
                        itemExt.item = item
                        itemExt.feedTitle = feed.title
                        self.parsedData.append(itemExt)
                    }
                }
                
                self.loadNumber += 1
                if self.loadNumber == self.featureCount {
                    self.processAfterFeedsLoad()
                }
            }
//            loadRss(url)
        }
        if feedsSelected[3] {
            let url = URL(string: "http://cointelegraph.com/rss")!
            let parser = FeedParser(URL: url)
            parser.parseAsync(queue: DispatchQueue.global(qos: .userInitiated)) { (result) in
                guard let feed = result.rssFeed, result.isSuccess else {
                    print(result.error!)
                    return
                }
                if let items = feed.items {
                    for item in items {
                        let itemExt = RSSFeedItemExt()
                        itemExt.item = item
                        itemExt.feedTitle = feed.title
                        self.parsedData.append(itemExt)
                    }
                }
                
                self.loadNumber += 1
                if self.loadNumber == self.featureCount {
                    self.processAfterFeedsLoad()
                }
            }
//            loadRss(url)
        }
        
        
    }
    
//    func loadRss(_ data: URL) {
//
//        DispatchQueue.global().async {
//            self.parser = XMLParser(contentsOf: data)!
//            self.parser.delegate = self
//            let success:Bool = self.parser.parse()
//
//            if success {
//                print("parse success!")
//                print(self.loadNumber)
//                self.loadNumber += 1
//                self.parsedData.append(self.currentData)
//
//                if self.loadNumber == self.featureCount {
//                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.5) {
//                        self.parsedData.sort(by: {
//                            let dateFormatter = DateFormatter()
//                            dateFormatter.dateFormat = "E, d MMM yyyy HH:mm:ss Z"
//                            guard let date_0 = dateFormatter.date(from: $0["pubDate"]!),
//                                let date_1 = dateFormatter.date(from: $1["pubDate"]!) else {
//                                    return false
//                            }
//                            return date_0.compare(date_1) == .orderedDescending
//                        })
//                    }
//
//                    if self.isReloading == true {
//                        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1) {
//                            self.isReloading = false
//                            self.tableView.tableHeaderView = nil
//                        }
//                        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1.1) {
//                            self.tableView.reloadData()
//                        }
//                    } else {
//                        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.6, execute: {
//                            self.dismissHUD(isAnimated: true)
//                            self.tableView.reloadData()
//                        })
//                    }
//
//                }
//            } else {
//                print("parse failure!")
//            }
//        }
//    }
//
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "openDetail" {
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == 0 {
           tableView.deselectRow(at: indexPath, animated: true)
            return
        }
        
        guard let cellData = parsedData[indexPath.row-1].item else {
           tableView.deselectRow(at: indexPath, animated: true)
            return
        }
        
        guard let urlString = cellData.link
        else {
            tableView.deselectRow(at: indexPath, animated: true)
            return
        }
        guard let url = URL(string: urlString) else {
            tableView.deselectRow(at: indexPath, animated: true)
            return
        }
//        UIApplication.shared.open(url)

//        urlString = urlString.replacingOccurrences(of: " ", with: "")
//        urlString = urlString.replacingOccurrences(of: "\n", with: "")
//
        let config = SFSafariViewController.Configuration()
        config.entersReaderIfAvailable = true
        tableView.deselectRow(at: indexPath, animated: true)
        
        let vc = SFSafariViewController(url: url, configuration: config)
        vc.preferredBarTintColor = UIColor(red: 3.0/255,green: 73.0/255,blue: 184.0/255,alpha: 1.0)
        vc.preferredControlTintColor = UIColor.white
        vc.delegate = self as? SFSafariViewControllerDelegate
        self.present(vc, animated: true,completion: nil)
//
//        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offset = scrollView.contentOffset.y
        if offset < 0 && isReloading == false {
            isReloading = true
            let spinner = UIActivityIndicatorView(style: .gray)
            spinner.startAnimating()
            spinner.frame = CGRect(x: 0, y: 0, width: self.tableView.frame.width, height: 44)
            self.tableView.tableHeaderView = spinner;
            self.loadData()
        }
    }
    
    
    func safariViewControllerFinish(_ controller: SFSafariViewController)
    {
        controller.dismiss(animated: true, completion: nil)
    }
    
    // MARK: - Table view data source.
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return parsedData.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "headerCell", for: indexPath) as! NewsTableHeaderCell
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! NewsTableViewCell
        
        
        cell.tempNewsImage.layer.cornerRadius = 42
        cell.tempNewsImage.layer.masksToBounds = true
        
        if parsedData.count == 0 {
            return cell
        }
        
        guard let cellData = parsedData[indexPath.row-1].item,
                let headerTitle = parsedData[indexPath.row-1].feedTitle
        else {
            return cell
        }
        guard let title = cellData.title else {
            return cell
        }
    
        if let image = self._imageList[title] {
            cell.tempNewsImage.image = image
        } else {

            if let media = cellData.media,
                let contents = media.mediaContents,
                contents.count >= 1,
                let attr = contents[0].attributes,
                let contentUrlStr = attr.url,
                let contentUrl = URL(string: contentUrlStr)
            {
                DispatchQueue.global().async {
                    var image: UIImage?
                    let data = NSData(contentsOf: contentUrl)
                    do {
                        image = UIImage(data:data! as Data)
                    }
                    DispatchQueue.main.async(execute: {
                        if let img = image {
                            image = self.resizeImage(image: img, toTheSize: CGSize(width: 100, height: 100))
                            self._imageList[title] = image!
                            cell.tempNewsImage.image = image
                        }
                    })
                }
            } else {
//                let frame = CGRect(x: 0, y: 0, width: 100, height: 100)
//                let nameLabel = UILabel(frame: frame)
//                nameLabel.textAlignment = .center
//                nameLabel.backgroundColor = .lightGray
//                nameLabel.textColor = .white
//                nameLabel.font = UIFont.boldSystemFont(ofSize: 15)
//                nameLabel.text = headerTitle
//                UIGraphicsBeginImageContext(frame.size)
//                if let currentContext = UIGraphicsGetCurrentContext() {
//                    nameLabel.layer.render(in: currentContext)
//                    let nameImage = UIGraphicsGetImageFromCurrentImageContext()!
//                    self._imageList[title] = nameImage
//                    cell.tempNewsImage.image = nameImage
//                }
                if headerTitle.lowercased() == "coindesk" {
                    cell.tempNewsImage.image = UIImage(named: "coindesk")
                } else if headerTitle.lowercased() == "bitcoin news" {
                    cell.tempNewsImage.image = UIImage(named: "bitcoin_news")
                }
            }
        }
        if (headerTitle.lowercased().contains("cointelegraph")) {
            cell.firstLabel.text = "Cointelegraph"
        } else if (headerTitle.lowercased().contains("bitcoinist")) {
            cell.firstLabel.text = "Bitcoinist"
        } else {
            cell.firstLabel.text = headerTitle
        }

        cell.secondLabel.text = title
        if let date = cellData.pubDate {
            cell.dateLabel.text = relativePast(for: date)
        }
        
//        guard let link = cellData["link"] else {
//            return cell
//        }
//
//
//        var image: UIImage? = nil
//
//        if let image = _imageList[link] {
//            cell.tempNewsImage.image = image
//        } else {
//
//            if let url = URL(string: link),
//                let data = NSData(contentsOf:url as URL){
//                do {
//                    image = UIImage(data:data as Data)
//                }
//                DispatchQueue.main.async(execute: {
//                    if let img = image {
//                        image = self.resizeImage(image: img, toTheSize: CGSize(width: 100, height: 100))
//                        cell.tempNewsImage.image = image
//                        self._imageList[link] = image
//                    }
//                })
//            }
//
//        }
//

//        if (cellData["header_title"]?.lowercased().contains("cointelegraph"))! {
//            cell.firstLabel.text = "Cointelegraph"
//        } else if (cellData["header_title"]?.lowercased().contains("bitcoinist"))! {
//            cell.firstLabel.text = "Bitcoinist"
//        } else {
//            cell.firstLabel.text = cellData["header_title"]
//        }
//
//        cell.secondLabel.text = cellData["title"]
//
//        let dateFormatter = DateFormatter()
//        dateFormatter.dateFormat = "E, d MMM yyyy HH:mm:ss Z"
//        if let date = dateFormatter.date(from: cellData["pubDate"]!){
//            cell.dateLabel.text = relativePast(for: date)
//        }
    
        return cell
    }
    
    func UIColorFromRGB(rgbValue: UInt) -> UIColor {
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    func resizeImage(image:UIImage, toTheSize size:CGSize)->UIImage{
        
        let scale = CGFloat(max(size.width/image.size.width,
                                size.height/image.size.height))
        let width:CGFloat  = image.size.width * scale
        let height:CGFloat = image.size.height * scale;
        
        let rr:CGRect = CGRect(x: 0, y: 0, width: width, height: height)
        
        UIGraphicsBeginImageContextWithOptions(size, false, 0);
        image.draw(in: rr)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext();
        return newImage!
    }
    
    func setupTitleView() {
        
        let textAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white]
        
        navigationController?.navigationBar.barTintColor = UIColor(red: 3.0/255,green: 99.0/255,blue: 184.0/255,alpha: 1.0)
        navigationController?.navigationBar.titleTextAttributes = textAttributes
        navigationController?.navigationBar.tintColor = UIColor.white
        navigationController?.navigationBar.isTranslucent = false
        
        let f = DateFormatter()
        let weekDay = f.shortWeekdaySymbols[Calendar.current.component(.weekday, from: Date()) - 1]
        let day = Calendar.current.component(.day, from: Date())
        
        let topText = NSLocalizedString(weekDay.uppercased(), comment: "")
        let bottomText = NSLocalizedString(String(day), comment: "")
        
        let titleParameters = [NSAttributedString.Key.foregroundColor : UIColor.black,
                               NSAttributedString.Key.font : UIFont.systemFont(ofSize: 9.0)]
        let subtitleParameters = [NSAttributedString.Key.foregroundColor : UIColor.black,
                                  NSAttributedString.Key.font : UIFont.systemFont(ofSize: 15.0)]
        
        let title:NSMutableAttributedString = NSMutableAttributedString(string: topText, attributes: titleParameters)
        let subtitle:NSAttributedString = NSAttributedString(string: bottomText, attributes: subtitleParameters)
        
        title.append(NSAttributedString(string: "\n"))
        title.append(subtitle)
        
        //let size = title.size()
        
        //let width = size.width
        guard let height = navigationController?.navigationBar.frame.size.height else {return}
        
        let titleLabel = UILabel(frame: CGRect(x: 0,y: 0, width: height - 8, height: height - 8))
        titleLabel.attributedText = title
        titleLabel.numberOfLines = 0
        titleLabel.textAlignment = .center
        
        titleLabel.backgroundColor = UIColor.white
        titleLabel.layer.masksToBounds = true
        titleLabel.layer.cornerRadius = 5
        
        let leftBarButton = UIBarButtonItem(customView: titleLabel)
        navigationItem.leftBarButtonItem = leftBarButton
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(leftButtonTapped(_:)))
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.font: UIFont.systemFont(ofSize: 20, weight: UIFont.Weight.heavy)]
        
    }
    
    @objc func leftButtonTapped(_ sender: UIBarButtonItem!) {
        let Storyboard = UIStoryboard(name: "Main", bundle: nil)
        let DvC = Storyboard.instantiateViewController(withIdentifier: "selectFeeds") as! SelectFeedsTableViewController

        self.navigationController?.pushViewController(DvC, animated: true)
    }
    
    func relativePast(for date : Date) -> String {
        
        let units = Set<Calendar.Component>([.year, .month, .day, .hour, .minute, .second, .weekOfYear])
        let components = Calendar.current.dateComponents(units, from: date, to: Date())
        
        if components.year! > 0 {
            return "\(components.year!) " + (components.year! > 1 ? "years ago" : "year ago")
            
        } else if components.month! > 0 {
            return "\(components.month!) " + (components.month! > 1 ? "months ago" : "month ago")
            
        } else if components.weekOfYear! > 0 {
            return "\(components.weekOfYear!) " + (components.weekOfYear! > 1 ? "weeks ago" : "week ago")
            
        } else if (components.day! > 0) {
            return (components.day! > 1 ? "\(components.day!) days ago" : "Yesterday")
            
        } else if components.hour! > 0 {
            return "\(components.hour!) " + (components.hour! > 1 ? "hours ago" : "hour ago")
            
        } else if components.minute! > 0 {
            return "\(components.minute!) " + (components.minute! > 1 ? "minutes ago" : "minute ago")
            
        } else {
            return "\(components.second!) " + (components.second! > 1 ? "seconds ago" : "second ago")
        }
    }

//    func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String]) {
//        currentElement=elementName;
//
//        if currentElement == "item" || currentElement == "entry" {
//            if isHeader == false {
//                parsedData.append(currentData)
//            }
//            isHeader = false
//        }
//        if currentElement == "channel" {
//            isHeader = true
//        }
//
//        if isHeader == false {
//
//            if currentElement == "media:content" || currentElement=="media:thumbnail" {
//                if let url = attributeDict["url"] {
//                     foundCharacters += url
//                }
//            } else {
//
//            }
//        }
//
//    }
//
//    func parser(_ parser: XMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
//        if !foundCharacters.isEmpty {
//            foundCharacters = foundCharacters.trimmingCharacters(in: .whitespacesAndNewlines)
//            currentData[currentElement] = foundCharacters
//            foundCharacters = ""
//        }
//    }
//
//    func parser(_ parser: XMLParser, foundCharacters string: String) {
//
//        if isHeader == false {
//
//            if currentElement == "title" || currentElement == "link" || currentElement == "pubDate"
//           {
//                foundCharacters += string
//                foundCharacters = foundCharacters.deleteHTMLTags(tags: ["a", "p", "div", "img"])
//            }
//        }
//
//        if isHeader == true {
//            if currentElement == "title" {
//                currentElement = "header_title"
//                foundCharacters += string
//                foundCharacters = foundCharacters.deleteHTMLTags(tags: ["a", "p", "div", "img"])
//            }
//        }
//    }
//
//    func parser(_ parser: XMLParser, parseErrorOccurred parseError: Error) {
//        print("failure error: ", parseError)
//    }

}

extension String {
    func deleteHTMLTag(tag:String) -> String {
        return self.replacingOccurrences(of: "(?i)</?\(tag)\\b[^<]*>", with: "", options: .regularExpression, range: nil)
    }
    
    func deleteHTMLTags(tags:[String]) -> String {
        var mutableString = self
        for tag in tags {
            mutableString = mutableString.deleteHTMLTag(tag: tag)
        }
        return mutableString
    }
}
